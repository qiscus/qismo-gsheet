from app.webhook import Webhook
from app.gsheet import setup_gsheet
from app.settings import SETTINGS
from fastapi import FastAPI, BackgroundTasks

app = FastAPI()

sheet = setup_gsheet(SETTINGS.service_account, SETTINGS.spreadsheet_key, SETTINGS.sheet_name)

@app.get("/")
async def root():
    return "ok"

@app.post("/webhooks")
async def webhooks(webhook: Webhook, job: BackgroundTasks):
    complain = "No complain data"
    address = "No address data"

    additional_infos = webhook.additional_info
    for info in additional_infos:
        if info.key.lower() == "keluhan":
            complain = info.value

        if info.key.lower() == "alamat":
            address = info.value

    values = [
        webhook.customer.name,
        webhook.customer.user_id,
        address,
        complain
    ]

    job.add_task(_append_row, values)

    return "ok"


def _append_row(values: list):
    sheet.append_row(values, insert_data_option="INSERT_ROWS")
