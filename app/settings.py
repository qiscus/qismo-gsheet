from pydantic import BaseSettings

class NewSettings(BaseSettings):
    service_account: dict
    spreadsheet_key: str
    sheet_name: str


    class Config:
        env_file = ".env"


SETTINGS = NewSettings()
