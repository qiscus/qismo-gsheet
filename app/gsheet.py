from gspread.models import Worksheet
import gspread

def setup_gsheet(service_account: dict, spreadsheet_key: str, sheet_name: str) -> Worksheet:
    client = gspread.service_account_from_dict(service_account)
    spreadsheet = client.open_by_key(spreadsheet_key)
    sheet = spreadsheet.worksheet(sheet_name)

    return sheet
