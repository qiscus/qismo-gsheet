gspread==3.7.0
fastapi==0.65.3
python-dotenv==0.18.0
uvicorn==0.14.0
